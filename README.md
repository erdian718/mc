# cache

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/erdian718/cache.svg)](https://pkg.go.dev/gitlab.com/erdian718/cache)
[![Go Version](https://img.shields.io/badge/go%20version-%3E=1.20-61CFDD.svg?style=flat-square)](https://go.dev/)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/erdian718/cache)](https://goreportcard.com/report/gitlab.com/erdian718/cache)

Package cache implements a simple in-memory cache.

## Feature

* Capacity limitation.
* Time to live setting.
* Progressive release of memory.
* Goroutine safe.

## Usage

```go
import (
	"time"

	"gitlab.com/erdian718/cache"
)

func main() {
	c := cache.New[string, int](1024, 15*time.Minute)

	c.Store("A", 1)
	c.Store("B", 2)

	c.Load("A")
	c.Load("B")
	c.Load("C")

	// ...
}
```

## Note

* Although it is thread safe, the presence of locks does not fully utilize multi-core performance.
* Although reusing or clearing expired entries as much as possible, the scaling mechanism depends on the `map` implementation.
