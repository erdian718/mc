package cache

type entry[K comparable, V any] struct {
	key   K
	value V
	stamp int64

	prev *entry[K, V]
	next *entry[K, V]
}
