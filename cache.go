// Package cache implements a simple in-memory cache.
package cache

import (
	"math"
	"sync"
	"time"
)

// Map represents a in-memory cache map.
type Map[K comparable, V any] struct {
	mutex   sync.Mutex
	cap     int
	ttl     int64
	head    entry[K, V]
	tail    entry[K, V]
	entries map[K]*entry[K, V]
}

// New creates a new in-memory cache map by capacity and time to live.
// The minimum unit of ttl is second.
func New[K comparable, V any](cap int, ttl time.Duration) *Map[K, V] {
	// NOTE: ensure minimum parameters.
	if cap < 1 {
		cap = 1
	}
	if ttl < time.Second {
		ttl = time.Second
	}

	m := Map[K, V]{
		cap:     cap,
		ttl:     int64(ttl / time.Second),
		entries: make(map[K]*entry[K, V]),
	}
	m.head.stamp = math.MaxInt64 // NOTE: head never expires.
	m.tail.stamp = math.MaxInt64 // NOTE: tail never expires.
	m.head.next = &m.tail
	m.tail.prev = &m.head
	return &m
}

// Cap returns the map capacity.
func (m *Map[K, V]) Cap() int {
	return m.cap
}

// TTL returns the map ttl.
func (m *Map[K, V]) TTL() time.Duration {
	return time.Duration(m.ttl) * time.Second
}

// Len returns the map length.
func (m *Map[K, V]) Len() int {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	return len(m.entries)
}

// Delete deletes the value for a key.
func (m *Map[K, V]) Delete(key K) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	if e, ok := m.entries[key]; ok {
		delete(m.entries, key)
		m.remove(e)
	}
}

// Load returns the value stored in the map for a key.
// The ok result indicates whether value was found in the map.
func (m *Map[K, V]) Load(key K) (value V, ok bool) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	e, ok := m.entries[key]
	if !ok {
		return
	}

	now := time.Now().Unix()
	if e.stamp < now {
		delete(m.entries, key)
		m.remove(e)
		return value, false
	}

	m.active(e, now)
	return e.value, true
}

// Store sets the value for a key.
func (m *Map[K, V]) Store(key K, value V) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	now := time.Now().Unix()
	if e, ok := m.entries[key]; ok {
		e.value = value
		m.active(e, now)
		return
	}

	e := m.require(now)
	e.key = key
	e.value = value
	m.insert(e)
	m.entries[key] = e
}

func (m *Map[K, V]) remove(e *entry[K, V]) {
	e.prev.next = e.next
	e.next.prev = e.prev
}

func (m *Map[K, V]) insert(e *entry[K, V]) {
	e.prev = &m.head
	e.next = m.head.next
	m.head.next = e
	e.next.prev = e
}

func (m *Map[K, V]) active(e *entry[K, V], now int64) {
	if e.prev != &m.head {
		m.remove(e)
		m.insert(e)
	}
	e.stamp = now + m.ttl
}

func (m *Map[K, V]) require(now int64) *entry[K, V] {
	e := m.tail.prev
	if e.stamp < now {
		delete(m.entries, e.key)

		// NOTE: progressive release of memory.
		x := e.prev
		if x.stamp < now {
			delete(m.entries, x.key)
			e = x
		}

		e.prev.next = &m.tail
		m.tail.prev = e.prev
	} else if len(m.entries) >= m.cap {
		delete(m.entries, e.key)
		m.remove(e)
	} else {
		e = new(entry[K, V])
	}
	e.stamp = now + m.ttl
	return e
}
