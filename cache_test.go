package cache_test

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/erdian718/cache"
)

func Example() {
	c := cache.New[string, int](1024, 15*time.Minute)

	c.Store("A", 1)
	c.Store("B", 2)

	fmt.Println(c.Load("A"))
	fmt.Println(c.Load("B"))
	fmt.Println(c.Load("C"))
	// Output:
	// 1 true
	// 2 true
	// 0 false
}

func TestCap(t *testing.T) {
	const cap = 1234
	c := cache.New[string, int](cap, 0)
	if c.Cap() != cap {
		t.Fail()
	}

	c = cache.New[string, int](0, 0)
	if c.Cap() < 1 {
		t.Fail()
	}

	c = cache.New[string, int](-1, 0)
	if c.Cap() < 1 {
		t.Fail()
	}
}

func TestTTL(t *testing.T) {
	const ttl = 1234 * time.Second
	c := cache.New[string, int](0, ttl)
	if c.TTL() != ttl {
		t.Fail()
	}

	c = cache.New[string, int](0, 0)
	if c.TTL() < time.Second {
		t.Fail()
	}

	c = cache.New[string, int](0, -time.Second)
	if c.TTL() < time.Second {
		t.Fail()
	}
}

func TestLen(t *testing.T) {
	c := cache.New[string, int](2, time.Minute)
	if c.Len() != 0 {
		t.Fail()
	}

	c.Store("A", 1)
	if c.Len() != 1 {
		t.Fail()
	}

	c.Store("A", -1)
	if c.Len() != 1 {
		t.Fail()
	}

	c.Store("B", 2)
	if c.Len() != 2 {
		t.Fail()
	}

	c.Store("C", 3)
	if c.Len() != 2 {
		t.Fail()
	}
}

func TestDelete(t *testing.T) {
	cache := cache.New[string, int](1024, time.Minute)
	cache.Store("A", 1)

	cache.Delete("A")
	if _, ok := cache.Load("A"); ok {
		t.Fail()
	}

	cache.Delete("B")
	if _, ok := cache.Load("B"); ok {
		t.Fail()
	}
}

func TestStoreAndLoad(t *testing.T) {
	cache := cache.New[string, int](1024, time.Minute)
	cache.Store("A", 1)
	cache.Store("B", 2)

	if v, ok := cache.Load("A"); !ok || v != 1 {
		t.Fail()
	}
	if v, ok := cache.Load("B"); !ok || v != 2 {
		t.Fail()
	}
	if _, ok := cache.Load("C"); ok {
		t.Fail()
	}
}

func TestExpire(t *testing.T) {
	cache := cache.New[string, int](4, 3*time.Second)
	cache.Store("A", 1)
	cache.Store("B", 2)
	cache.Store("C", 3)
	cache.Store("D", 4)

	time.Sleep(2 * time.Second)
	if v, ok := cache.Load("D"); !ok || v != 4 {
		t.Fail()
	}

	time.Sleep(2 * time.Second)
	if _, ok := cache.Load("C"); ok {
		t.Fail()
	}
	cache.Store("E", 5)
	cache.Store("F", 6)
	cache.Store("G", 7)
	cache.Store("H", 8)

	if cache.Len() != cache.Cap() {
		t.Fail()
	}
	if _, ok := cache.Load("A"); ok {
		t.Fail()
	}
	if _, ok := cache.Load("B"); ok {
		t.Fail()
	}
	if _, ok := cache.Load("C"); ok {
		t.Fail()
	}
	if _, ok := cache.Load("D"); ok {
		t.Fail()
	}
	if v, ok := cache.Load("E"); !ok || v != 5 {
		t.Fail()
	}
	if v, ok := cache.Load("F"); !ok || v != 6 {
		t.Fail()
	}
	if v, ok := cache.Load("G"); !ok || v != 7 {
		t.Fail()
	}
	if v, ok := cache.Load("H"); !ok || v != 8 {
		t.Fail()
	}
}

func BenchmarkStore(b *testing.B) {
	cache := cache.New[int, int](1024, time.Second)
	b.RunParallel(func(pb *testing.PB) {
		i := 0
		for pb.Next() {
			cache.Store(i, i)
			i++
			if i >= 1024 {
				i = 0
			}
		}
	})
}

func BenchmarkLoad(b *testing.B) {
	cache := cache.New[int, int](1024, time.Second)
	for i := 0; i < 1024; i++ {
		cache.Store(i, i)
	}

	b.RunParallel(func(pb *testing.PB) {
		i := 0
		for pb.Next() {
			cache.Store(i, i)
			i++
			if i >= 1024 {
				i = 0
			}
		}
	})
}
